package com.example.eshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
EditText email,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email =findViewById(R.id.email);
        password =findViewById(R.id.password);

    }

    public void home(View view) {
        Intent intent =getIntent();
        String e=intent.getStringExtra("email");
        String p=intent.getStringExtra("password");
        if (email.getText().toString().isEmpty())
        {
            email.setError("Enter email address");
            email.requestFocus();
        }
        else if (password.getText().toString().isEmpty())
        {
            password.setError("enter password");
            password.requestFocus();

        }
        else if (e.equals(email.getText().toString()) && p.equals(password.getText().toString()))
        {
            startActivity(new Intent(this,HomePage.class));
            Toast.makeText(getApplicationContext(),"HomePage",Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getApplicationContext(),"Wrong Email Address or Password",Toast.LENGTH_SHORT).show();
        }
    }
}
