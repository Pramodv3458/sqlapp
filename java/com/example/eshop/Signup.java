package com.example.eshop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Signup extends AppCompatActivity {
EditText email,password,name,mob,repassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        email =findViewById(R.id.email);
        password =findViewById(R.id.password);
        mob = findViewById(R.id.mob);
        name =findViewById(R.id.name);
        repassword =findViewById(R.id.repassword);
    }

    public void login(View view) {
        if (name.getText().toString().isEmpty()) {
            name.setError("enter name ");
            name.requestFocus();
        }
       else if (email.getText().toString().isEmpty()) {
            email.setError("Enter email address");
            email.requestFocus();
        } else if (password.getText().toString().isEmpty()) {
            password.setError("enter password");
            password.requestFocus();
        } else if (!repassword.getText().toString() .equals( password.getText().toString())) {
            repassword.setError("not match password");
            repassword.requestFocus();
        } else if (mob.getText().toString().isEmpty()) {
            mob.setError("enter mobile number");
            mob.requestFocus();
        } else {
            String e = email.getText().toString();
            String p = password.getText().toString();
            Intent intent = new Intent(Signup.this, MainActivity.class);
            intent.putExtra("email", e);
            intent.putExtra("password", p);
            startActivity(intent);
        }

    }
    public void log(View view)
    {
        startActivity(new Intent(this,MainActivity.class));
    }
}
